const { expect } = require('chai');
require('../config');
const ShoppingList = require('./ShoppingList');

describe('ShoppingList', () => {
  it('does not allow lists without a name', (done) => {
    const list = new ShoppingList();
    list.validate((err) => {
      expect(err.errors.name).to.exist;
      done();
    });
  });

  describe('#items', () => {
    it('requires a name for the list items', (done) => {
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: '' }]
      });

      list.validate((err) => {
        expect(err.errors['items.0.name']).to.exist;
        done();
      });
    });

    it('isChecked fields default to false', () => {
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: 'Milk' }]
      });

      expect(list.items[0].isChecked).to.be.false;
    });

    it('ammount fields default to 1', () => {
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: 'Milk' }]
      });

      expect(list.items[0].ammount).to.equal(1);
    });
  });
});
