const proxyquire = require('proxyquire');
const sinon = require('sinon');
const supertest = require('supertest');
const { expect } = require('chai');
const express = require('express');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const ShoppingList = require('../models/ShoppingList');

/**
 * Unit tests for the shoppingLists controller.
 * Data layer is stubbed using the pattern found in http://evanshortiss.com/development/javascript/2016/04/15/express-testing-using-ioc.html.
 *
 * Uses a mock express app
 */
describe('shoppingLists controller', () => {
  let app;
  let request;
  let controller;
  let findAllStub;
  let saveStub;
  let findByIdStub;
  let removeStub;

  beforeEach(() => {
    // prepare stubs
    findAllStub = sinon.stub();
    saveStub = sinon.stub();
    findByIdStub = sinon.stub();
    removeStub = sinon.stub();

    app = express();
    app.use(bodyParser.json());
    app.use(expressValidator());
    // stub data access modules in the respository
    controller = proxyquire('./shoppingLists.js', {
      '../models/shoppingListRepository': {
        findAll: findAllStub,
        save: saveStub,
        findById: findByIdStub,
        remove: removeStub
      }
    });
    app.get('/api/shopping_lists', controller.getShoppingLists);
    app.post('/api/shopping_lists', controller.createList);
    app.delete('/api/shopping_lists/:id', controller.removeList);
    request = supertest(app);
  });

  describe('#getShoppingLists', () => {
    it('should return a list of shopping lists', (done) => {
      const listList = [new ShoppingList({ name: 'My list' })];
      findAllStub.returns(Promise.resolve(listList));

      request
        .get('/api/shopping_lists')
        .expect('Content-Type', /json/)
        .expect(200, (err, res) => {
          expect(res.body[0].name).to.equal('My list');
          done();
        });
    });
  });

  describe('#createList', () => {
    it('should succeed creating a list', (done) => {
      const data = { name: 'My list' };
      const shoppingList = new ShoppingList(data);
      saveStub.returns(Promise.resolve(shoppingList));

      request
        .post('/api/shopping_lists')
        .send(data)
        .expect('Content-Type', /json/)
        .expect(201, (err, res) => {
          expect(saveStub.called).to.be.true;
          expect(res.body.name).to.equal(data.name);
          expect(res.header.location).not.to.undefined;
          done();
        });
    });

    it('should handle validation errors', (done) => {
      request
        .post('/api/shopping_lists')
        .send({})
        .expect('Content-Type', /json/)
        .expect(400, (err, res) => {
          expect(saveStub.called).to.be.false;

          const { errors } = res.body;
          expect(errors.find(e => e.param === 'name')).not.to.undefined;

          done();
        });
    });

    it('should handle internal errors', (done) => {
      // logs an UnhandledPromiseRejectionWarning
      saveStub.returns(Promise.reject('internal error'));

      request
        .post('/api/shopping_lists')
        .send({})
        .expect('Content-Type', /json/)
        .expect(500, () => done());
    });
  });

  describe('#removeList', () => {
    it('returns a 204 response on success', (done) => {
      const list = new ShoppingList();
      findByIdStub.returns(Promise.resolve(list));
      removeStub.returns(Promise.resolve());

      request
        .delete('/api/shopping_lists/123123123123123')
        .expect(204, (err, res) => {
          expect(res.body).to.be.empty;
          done();
        });
    });

    it('returns 404 when the list is not found', (done) => {
      findByIdStub.returns(Promise.resolve(null));

      request.delete('/api/shopping_lists/123123123123123').expect(404, () => {
        expect(removeStub.called).to.be.false;
        done();
      });
    });
  });
});
