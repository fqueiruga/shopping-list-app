const { expect } = require('chai');
require('../config');
const ShoppingList = require('./ShoppingList');
const {
  findAll,
  save,
  findById,
  remove
} = require('../models/shoppingListRepository');

/**
 * Tests for the ShoppingListRepository that wraps the DB operations of this model.
 * These tests hit the DB.
 */
describe('shoppingListRepository', () => {
  let list;
  let invalidList;

  beforeEach(() => {
    list = new ShoppingList({ name: 'My list' });
    invalidList = new ShoppingList();
  });

  afterEach(() => {
    ShoppingList.collection.remove();
  });

  describe('#findAll', () => {
    it('returns an array of lists', (done) => {
      list.save().then(() => {
        findAll().then((lists) => {
          expect(lists).not.to.be.empty;
          expect(lists.find(c => c._id.toString() === list._id.toString())).not
            .to.undefined;
          done();
        });
      });
    });

    it('returns an empty array when there are no lists', (done) => {
      findAll().then((lists) => {
        expect(lists).to.eql([]);
        done();
      });
    });
  });

  describe('#findById', () => {
    it('retrieves a list', (done) => {
      list.save().then(() => {
        findById(list._id.toString()).then((foundList) => {
          expect(foundList._id).to.eql(list._id);
          expect(foundList.name).to.eql(list.name);
          done();
        });
      });
    });
  });

  describe('#save', () => {
    it('creates a new list', (done) => {
      save(list).then((savedList) => {
        expect(savedList._id).not.to.undefined;
        expect(savedList.name).eql(list.name);
        done();
      });
    });

    it('updates an existing list', (done) => {
      list
        .save()
        .then((existingList) => {
          existingList.name = 'New name';
          existingList.items.push({ name: 'Milk', ammount: 2 });
          return save(existingList);
        })
        .then((updatedList) => {
          expect(updatedList.name).to.equal('New name');
          expect(updatedList.items[0].name).to.equal('Milk');
          expect(updatedList.items[0].ammount).to.equal(2);
          done();
        });
    });

    it('updates a list item', (done) => {
      list.items.push({ name: 'Milk', ammount: 1 });
      list
        .save()
        .then((existingList) => {
          existingList.items[0].ammount = 2;
          existingList.items[0].isChecked = true;
          return save(existingList);
        })
        .then((updatedList) => {
          expect(updatedList.items[0].ammount).to.equal(2);
          expect(updatedList.items[0].isChecked).to.be.true;
          done();
        });
    });

    it('rejects the promise when encountering an error', (done) => {
      save(invalidList).catch((err) => {
        expect(err).not.to.undefined;
        expect(err.name).to.eql('ValidationError');
        done();
      });
    });
  });

  describe('#remove', () => {
    it('removes a list from the collection', (done) => {
      remove(list).then(done).catch((err) => {
        expect(err).to.undefined;
        done();
      });
    });
  });
});
