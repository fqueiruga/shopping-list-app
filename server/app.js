const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const expressValidator = require('express-validator');
require('./config');

/**
 * Create Express server.
 */
const app = express();

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

/**
 * API routes.
 */
const listsController = require('./controllers/shoppingLists');
const listItemsController = require('./controllers/shoppingListItems');

app.get('/api/shopping_lists', listsController.getShoppingLists);
app.post('/api/shopping_lists', listsController.createList);
app.delete('/api/shopping_lists/:id', listsController.removeList);
app.post('/api/shopping_lists/:listId/items', listItemsController.addItem);
app.put(
  '/api/shopping_lists/:listId/items/:id',
  listItemsController.updateItem
);
app.delete(
  '/api/shopping_lists/:listId/items/:id',
  listItemsController.removeItem
);

/**
 * Error Handler.
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log(
    '%s App is running at http://localhost:%d in %s mode',
    chalk.green('✓'),
    app.get('port'),
    app.get('env')
  );

  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
