const proxyquire = require('proxyquire');
const sinon = require('sinon');
const supertest = require('supertest');
const { expect } = require('chai');
const express = require('express');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');
const ShoppingList = require('../models/ShoppingList');

/**
 * Unit tests for the shoppingListItems controller.
 * Data layer is stubbed using the pattern found in http://evanshortiss.com/development/javascript/2016/04/15/express-testing-using-ioc.html.
 *
 * Uses a mock express app
 */
describe('shoppingListItems controller', () => {
  let app;
  let request;
  let findByIdStub;
  let saveStub;

  beforeEach(() => {
    // prepare stubs
    findByIdStub = sinon.stub();
    saveStub = sinon.stub();

    app = express();
    app.use(bodyParser.json());
    app.use(expressValidator());

    // stub data access modules
    const controller = proxyquire('./shoppingListItems', {
      '../models/shoppingListRepository': {
        findById: findByIdStub,
        save: saveStub
      }
    });
    app.post('/api/shopping_lists/:listId/items', controller.addItem);
    app.put('/api/shopping_lists/:listId/items/:id', controller.updateItem);
    app.delete('/api/shopping_lists/:listId/items/:id', controller.removeItem);
    request = supertest(app);
  });

  describe('#addItem', () => {
    it('adds an item to the shopping list', (done) => {
      const listId = mongoose.Types.ObjectId();
      const list = new ShoppingList({ name: 'My list' });

      const data = {
        name: 'Milk',
        ammount: 2
      };

      findByIdStub.returns(Promise.resolve(list));
      saveStub.returns(Promise.resolve(Object.assign(list, { items: [data] })));

      request
        .post(`/api/shopping_lists/${listId.toHexString()}/items`)
        .send(data)
        .expect('Content-Type', /json/)
        .expect(201, (err, res) => {
          expect(findByIdStub.called).to.be.true;

          const item = res.body.items[0];
          expect(item.name).to.equal(data.name);
          expect(item.ammount).to.equal(data.ammount);

          expect(res.body.items[0]);
          done();
        });
    });

    it('handles validation errors', (done) => {
      const listId = mongoose.Types.ObjectId();

      request
        .post(`/api/shopping_lists/${listId.toString()}/items`)
        .send({})
        .expect('Content-Type', /json/)
        .expect(400, (err, res) => {
          expect(findByIdStub.called).to.be.false;

          const { errors } = res.body;
          expect(errors.find(e => e.param === 'name')).not.to.undefined;

          done();
        });
    });

    it('handles 404 errors', (done) => {
      const listId = mongoose.Types.ObjectId();
      findByIdStub.returns(Promise.resolve(null));

      request
        .post(`/api/shopping_lists/${listId.toHexString()}/items`)
        .send({})
        .expect('Content-Type', /json/)
        .expect(404, () => done());
    });
  });

  describe('#updateItem', () => {
    it('updates an item', (done) => {
      const listId = mongoose.Types.ObjectId();
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: 'Milk' }]
      });
      const itemId = list.items[0]._id;

      const data = {
        ammount: 2,
        isChecked: true
      };

      findByIdStub.returns(Promise.resolve(list));
      saveStub.returns(Promise.resolve(list));

      request
        .put(`/api/shopping_lists/${listId}/items/${itemId}`)
        .send(data)
        .expect('Content-Type', /json/)
        .expect(200, (err, res) => {
          const item = list.items[0];
          expect(res.body._id).to.eql(item._id.toString());
          expect(res.body.name).to.equal(item.name);
          expect(res.body.ammount).to.equal(item.ammount);
          expect(res.body.isChecked).to.equal(item.isChecked);

          done();
        });
    });

    it('handles validation errors', (done) => {
      const listId = mongoose.Types.ObjectId();
      const itemId = mongoose.Types.ObjectId();
      const invalidData = {
        ammount: 'NaN'
      };

      request
        .put(`/api/shopping_lists/${listId.toHexString()}/items/${itemId}`)
        .send(invalidData)
        .expect('Content-Type', /json/)
        .expect(400, (err, res) => {
          expect(findByIdStub.called).to.be.false;

          const { errors } = res.body;
          expect(errors.find(e => e.param === 'ammount')).not.to.undefined;

          done();
        });
    });

    it('handles 404 errors', (done) => {
      const listId = mongoose.Types.ObjectId();
      // id of an item that is not on the list
      const wrongItemId = mongoose.Types.ObjectId();
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: 'Milk' }]
      });
      findByIdStub.returns(Promise.resolve(list));

      request
        .put(`/api/shopping_lists/${listId.toHexString()}/items/${wrongItemId}`)
        .send({})
        .expect('Content-Type', /json/)
        .expect(404, () => done());
    });
  });

  describe('#removeItem', () => {
    it('removes a list item', (done) => {
      const listId = mongoose.Types.ObjectId();
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: 'Milk' }]
      });
      const itemId = list.items[0]._id;

      findByIdStub.returns(Promise.resolve(list));
      saveStub.returns(Promise.resolve(list));

      request
        .delete(`/api/shopping_lists/${listId.toHexString()}/items/${itemId}`)
        .expect(204, () => {
          done();
        });
    });

    it('handles 404 errors', (done) => {
      const listId = mongoose.Types.ObjectId();
      // id of an item that is not on the list
      const wrongItemId = mongoose.Types.ObjectId();
      const list = new ShoppingList({
        name: 'My list',
        items: [{ name: 'Milk' }]
      });
      findByIdStub.returns(Promise.resolve(list));

      request
        .delete(
          `/api/shopping_lists/${listId.toHexString()}/items/${wrongItemId}`
        )
        .expect('Content-Type', /json/)
        .expect(404, () => done());
    });
  });
});
