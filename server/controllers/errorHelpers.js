/**
 * Responds to a Validation error
 *
 * @param res The response object
 * @param errors The list of errors
 */
exports.validationErrorResponse = (res, errors) => {
  res.status(400);
  res.json({
    message: 'Validation error',
    errors
  });
};

/**
 * Responds to a 404 error
 *
 * @param res The response object
 */
exports.notFoundErrorResponse = (res) => {
  res.status(404);
  res.json({ message: 'Not found' });
};

/**
 * Responds to a 500 error
 *
 * @param res The response object
 * @param err
 */
exports.internalErrorResponse = (res, err) => {
  console.error('Internal error:', err);

  res.json({ message: 'Internal error' });
  res.status(500);
};
