const { findById, save } = require('../models/shoppingListRepository');
const {
  validationErrorResponse,
  notFoundErrorResponse
} = require('./errorHelpers');

/**
 * Adds an item to a shopping list.
 *
 * When successful, returns the shopping list as JSON and a 201 status code.
 * Returns a 400 error when the data provided is not valid.
 * Returns a 404 error when it cannot find the list.
 */
exports.addItem = (req, res) => {
  req.checkBody('name', 'Name is required').notEmpty();
  req.checkBody('ammount', 'Invalid ammount').optional().isInt();
  req.sanitizeBody('isChecked').toBoolean();
  const errors = req.validationErrors();
  if (errors) {
    validationErrorResponse(res, errors);
    return;
  }

  findById(req.params.listId)
    .then((list) => {
      if (list === null) throw new Error('Not found');
      list.items.push(req.body);
      return save(list);
    })
    .then((updatedList) => {
      res.status(201);
      res.location('/api/shopping_lists');
      res.json(updatedList);
    })
    .catch(() => notFoundErrorResponse(res));
};

/**
 * Updates the information of an item in a shopping list.
 *
 * When successful, returns the item as JSON and a 200 status code.
 * Returns a 400 error when the data provided is not valid.
 * Returns a 404 error when it cannot find the list or the item.
 */
exports.updateItem = (req, res) => {
  req.checkBody('name', 'Invalid name').optional().notEmpty();
  req.checkBody('ammount', 'Invalid ammount').optional().isInt();
  req.sanitizeBody('isChecked').toBoolean();
  const errors = req.validationErrors();
  if (errors) {
    validationErrorResponse(res, errors);
    return;
  }

  findById(req.params.listId)
    .then((list) => {
      if (list === null) throw new Error('Not found');
      const item = list.items.id(req.params.id);
      if (item === null) throw new Error('Not found');

      // update the item data and update the parent document
      item.set(req.body);
      return save(list);
    })
    .then((list) => {
      res.status(200);
      res.json(list.items.id(req.params.id));
    })
    .catch(() => notFoundErrorResponse(res));
};

/**
 * Removes an item from a shopping list
 *
 * When successful, returns a 204 status code with empty body
 * Returns a 404 error when it cannot find the list or the item.
 */
exports.removeItem = (req, res) => {
  findById(req.params.listId)
    .then((list) => {
      if (list === null) throw new Error('Not found');
      const item = list.items.id(req.params.id);
      if (item === null) throw new Error('Not found');

      // updates the items array by filtering out the item to remove
      list.items = list.items.filter(i => i._id !== item._id);
      return save(list);
    })
    .then(() => res.sendStatus(204))
    .catch(() => notFoundErrorResponse(res));
};
