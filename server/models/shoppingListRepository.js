const mongoose = require('mongoose');
const ShoppingList = require('./ShoppingList');

/**
 * Retrieves all shopping lists in the system
 *
 * @return {Promise}
 */
exports.findAll = () =>
  new Promise((resolve, reject) => {
    ShoppingList.find({}, (err, lists) => {
      if (err) reject(err);
      resolve(lists);
    });
  });

/**
 * Retrieves a ShoppingList given its _id
 *
 * @param {mongoose.Types.ObjectId}
 * @return {Promise}
 */
exports.findById = id =>
  new Promise((resolve, reject) => {
    const objectId = mongoose.Types.ObjectId(id);
    ShoppingList.findById(objectId, (err, list) => {
      if (err) reject(err);
      resolve(list);
    });
  });

/**
 * Persists a ShoppingList in the DB
 *
 * @param {ShoppingList} list
 * @return {Promise}
 */
exports.save = list =>
  new Promise((resolve, reject) => {
    list.save((err) => {
      if (err) reject(err);
      resolve(list);
    });
  });

/**
 * Removes a Shopping list from the collection
 *
 * @param {ShoppingList} list
 * @return {Promise}
 */
exports.remove = list =>
  new Promise((resolve, reject) => {
    ShoppingList.remove({ _id: list._id }, (err) => {
      if (err) reject(err);
      resolve();
    });
  });
