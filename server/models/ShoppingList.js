const mongoose = require('mongoose');


/**
 * List item model. Its attributes are:
 */
const listItemSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    isChecked: {
      type: Boolean,
      default: false
    },
    ammount: {
      type: Number,
      default: 1,
    }
  },
  { timestamps: true }
);

/**
 * Shopping List model
 */
const shoppingListSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    items: [listItemSchema]
  },
  { timestamps: true }
);

const ShoppingList = mongoose.model('ShoppingList', shoppingListSchema);
module.exports = ShoppingList;
