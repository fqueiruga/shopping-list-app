## Shopping List app

REST API for a shopping list app.

Repo started from [this boilerplate](https://github.com/sahat/hackathon-starter).


### Project structure

- **server**: folder where the express app resides.
- **server/app.js**: express app setup.
- **server/config**: express app configuration (loading of envvars, mongoose setup, etc).
- **server/models**: mongoose models and repository modules. DB operations are done through the repositories.
- **server/controllers**: express route handlers and controller utils.

### Tests

The modules have their tests located in their same folder, to keep related code close.

Model and repository tests hit the DB. Controller tests stub the interaction with the model layer.

There are several warnings about rejected promises that aren't being handled in the tests. To test for failures, there are several uses of `Promise.reject` with no following catch clauses.

### Running the app

To run or test the app locally, the first step is to have a MongoDB instance up and running. A dockerized instance can be started with `docker-compose -f docker-compose.local.yml up`

If using another MongoDB instance, edit the mongo URLs in the environment configuration files (.env.development, .env.test).

Running the app:

- **Starting the server:** `npm start`
- **Development server with watch mode:** `npm run startDev`
- **Tests:** `npm run test`

------

## API DOCS

#### Get shopping lists
Returns JSON data of all the shopping lists in the system

- **URL:** /api/shopping_lists

- **Method:** GET

- **URL Params:**

- **Responses**

    - **Success:**

      Code: 200

      Content:

      ```
      [
          {
              "_id": "594fdb63fdab0132cfc2645c",
              "updatedAt": "2017-06-25T16:57:43.731Z",
              "createdAt": "2017-06-25T15:48:51.551Z",
              "name": "list 1",
              "__v": 6,
              "items": [
                  {
                      "updatedAt": "2017-06-25T15:51:43.535Z",
                      "createdAt": "2017-06-25T15:51:43.535Z",
                      "name": "milk",
                      "_id": "594fdc0f2264ff3479ae4631",
                      "ammount": 1,
                      "isChecked": false
                  },
                  {
                      "updatedAt": "2017-06-25T16:57:43.731Z",
                      "createdAt": "2017-06-25T16:56:55.244Z",
                      "name": "paco",
                      "_id": "594feb57e3a4044b5216c730",
                      "ammount": 10,
                      "isChecked": false
                  }
              ]
          },
          {
              "_id": "594fdb65fdab0132cfc2645d",
              "updatedAt": "2017-06-25T15:48:53.273Z",
              "createdAt": "2017-06-25T15:48:53.273Z",
              "name": "list 2",
              "__v": 0,
              "items": []
          }
      ]
      ```
  
- **Sample call:**

    ```
      curl -X GET http://localhost:3000/api/shopping_lists 
    ```


#### Create a shopping list

Creates a shopping list

- **URL:** /api/shopping_lists

- **Method:** POST

- **Body params:**

  - name (STRING, required)

- **Responses**

    - **Success:**

      Code: 201

      Content:
      ```
        {
          "__v": 0,
          "updatedAt": "2017-06-25T15:48:53.273Z",
          "createdAt": "2017-06-25T15:48:53.273Z",
          "name": "list 2",
          "_id": "594fdb65fdab0132cfc2645d",
          "items": []
        }
      ```
    
    - **Validation error:**

      Code: 400

      Content:
      ```
        {
          "message": "Validation error",
          "errors": [
              {
                  "param": "name",
                  "msg": "Name is required",
                  "value": ""
              }
          ]
        }
      ```
    
  
- **Sample call:**

    ```
      curl -X POST \
        http://localhost:3000/api/shopping_lists \
        -H 'content-type: application/json' \
        -d '{
          "name": "Supermarket list"
        }'
    ```


#### Delete a shopping list

Deletes a shopping list and all its information

- **URL:** /api/shopping_lists/:id

- **Method:** DELETE

- **URL Params:**

  - id (integer, required)

- **Responses**

    - **Success:**

      Code: 204

    - **Not found error:**

      Code: 404

      Content:
      ```
        {"message":"Not found"}
      ```
  
- **Sample call:**

    ```
      curl -X DELETE http://localhost:3000/api/shopping_lists/594fdb63fdab0132cfc26477
    ```



#### Add an item to a shopping list

- **URL:** /api/shopping_lists/:listId/items

- **Method:** POST

- **URL params:**

  - listId (integer, required)

- **Body params:**

  - name (string, required)
  - ammount (number, default 1)
  - isChecked (boolean, default false)

- **Responses**

    - **Success:**

      Code: 201

      Content: returns the whole list

      ```
        {
            "_id": "594fdb63fdab0132cfc2645c",
            "updatedAt": "2017-06-25T16:57:43.731Z",
            "createdAt": "2017-06-25T15:48:51.551Z",
            "name": "list 1",
            "__v": 6,
            "items": [
                {
                    "updatedAt": "2017-06-25T15:51:43.535Z",
                    "createdAt": "2017-06-25T15:51:43.535Z",
                    "name": "milk",
                    "_id": "594fdc0f2264ff3479ae4631",
                    "ammount": 1,
                    "isChecked": false
                },
                {
                    "updatedAt": "2017-06-25T16:57:43.731Z",
                    "createdAt": "2017-06-25T16:56:55.244Z",
                    "name": "paco",
                    "_id": "594feb57e3a4044b5216c730",
                    "ammount": 10,
                    "isChecked": false
                }
            ]
        }
      ```

    - **Validation error:**

      Code: 400

      Content:
      ```
        {
            "message": "Validation error",
            "errors": [
                {
                    "param": "name",
                    "msg": "Name is required",
                    "value": ""
                },
                {
                    "param": "ammount",
                    "msg": "Invalid ammount",
                    "value": "NaN"
                }
            ]
        }
      ```

    - **Not found error:**

      Code: 404

      Content:
      ```
        {"message":"Not found"}
      ```

  
- **Sample call:**

    ```
      curl -X POST \
        http://localhost:3000/api/shopping_lists/594fdb63fdab0132cfc2645c/items \
        -H 'content-type: application/json' \
        -d '{
        "name": "water",
        "ammount": 2
      }'
    ```


#### Update a list item

Updates a list item. Mostly used to marked it as checked.

- **URL:** /api/shopping_lists/:listId/items/:id

- **Method:** PUT

- **URL params:**

  - listId (integer, required)
  - id (integer, required)

- **Body params:**

  - name (string)
  - ammount (number)
  - isChecked (boolean)

- **Responses**

    - **Success:**

      Code: 200

      Content: the updated item

      ```
        {
            "updatedAt": "2017-06-25T15:51:43.535Z",
            "createdAt": "2017-06-25T15:51:43.535Z",
            "name": "milk",
            "_id": "594fdc0f2264ff3479ae4631",
            "ammount": 1,
            "isChecked": false
        }
      ```

    - **Validation error:**

      Code: 400

      Content:
      ```
        {
            "message": "Validation error",
            "errors": [
                {
                    "param": "ammount",
                    "msg": "Invalid ammount",
                    "value": "NaN"
                }
            ]
        }
      ```

    - **Not found error:**

      Code: 404

      Content:
      ```
        {"message":"Not found"}
      ```
  
- **Sample call:**

    ```
      curl -X PUT \
        http://localhost:3000/api/shopping_lists/594fdb63fdab0132cfc2645c/items/594feb57e3a4044b5216c730 \
        -H 'content-type: application/json' \
        -d '{
        "isChecked": false,
        "ammount": 2
      }'
    ```



#### Delete a list item

- **URL:** /api/shopping_lists/:listId/items/:id

- **Method:** DELETE

- **URL params:**

  - listId (integer, required)
  - id (integer, required)

- **Responses**

    - **Success:**

      Code: 204
    
    - **Not found error:**

      Code: 404

      Content:
      ```
        {"message":"Not found"}
      ```
  
- **Sample call:**

    ```
      curl -X DELETE http://localhost:3000/api/shopping_lists/594fdb63fdab0132cfc2645c/items/594feb57e3a4044b5216c730
    ```
