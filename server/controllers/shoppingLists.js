const ShoppingList = require('../models/ShoppingList');
const {
  findAll,
  save,
  findById,
  remove
} = require('../models/shoppingListRepository');
const {
  validationErrorResponse,
  notFoundErrorResponse,
  internalErrorResponse
} = require('./errorHelpers');

/**
 * Returns an array of shopping lists
 *
 * Returns the list of items as JSON and a 200 status code
 */
exports.getShoppingLists = (req, res) => {
  findAll()
    .then(lists => res.json(lists))
    .catch(err => internalErrorResponse(res, err));
};

/**
 * Handles creating of a shopping list
 *
 * When successful, returns the list as JSON and a 201 status code.
 * Returns a 400 error when the data provided is not valid.
 */
exports.createList = (req, res) => {
  req.checkBody('name', 'Name is required').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    validationErrorResponse(res, errors);
    return;
  }

  const list = new ShoppingList(req.body);
  save(list)
    .then((list) => {
      res.status(201);
      res.location('/api/shopping_lists');
      res.json(list);
    })
    .catch(err => internalErrorResponse(res, err));
};

/**
 * Removes a list
 *
 * When successful, returns a 204 status code with empty body
 * Returns a 404 error when it cannot find the list.
 */
exports.removeList = (req, res) => {
  const id = req.params.id;
  findById(id)
    .then((list) => {
      if (list === null) throw new Error('Not found');
      return remove(list);
    })
    .then(() => res.sendStatus(204))
    .catch(() => notFoundErrorResponse(res));
};
